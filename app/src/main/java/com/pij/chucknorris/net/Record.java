package com.pij.chucknorris.net;

/**
 * @author Pierrejean on 02/05/2015.
 */
public class Record {

    private int id;
    private String joke;

    public Record(int id, String joke) {
        this.id = id;
        this.joke = joke;
    }

    public int getId() {
        return id;
    }

    public String getJoke() {
        return joke;
    }
}
