package com.pij.chucknorris;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Pierrejean on 04/05/2015.
 */
public class Util {

    public static <T> Observable.Transformer<T, T> netScheduler() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
