package com.pij.chucknorris.net;


import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * @author Pierrejean on 02/05/2015.
 */
public interface ChuckNorrisService {

    @GET("/jokes/{jokeId}")
    Observable<Response<Record>> getJoke(@Path("jokeId") int jokeId);

    @GET("/categories")
    Observable<Response<List<String>>> getCategories();
}

