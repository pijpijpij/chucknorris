package com.pij.chucknorris;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Toolbar;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class JokeActivity extends Activity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke);
        ButterKnife.inject(this);
        setActionBar(toolbar);

        if (savedInstanceState == null) {
            // First-time init; create fragment to embed in activity.
            add(JokeFragment.newInstance(1));
        }
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }

    private void add(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.container, fragment);
        ft.commit();
    }

}
