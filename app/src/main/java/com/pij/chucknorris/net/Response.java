package com.pij.chucknorris.net;

import android.support.annotation.NonNull;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * @author Pierrejean on 02/05/2015.
 */
public class Response<T> {

    private String type;
    private T value;

    public Response(@NonNull String type, @NonNull T value) {
        notNull(type);
        notNull(value);
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public T getValue() {
        return value;
    }
}
