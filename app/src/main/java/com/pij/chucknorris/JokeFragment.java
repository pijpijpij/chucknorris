package com.pij.chucknorris;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pij.chucknorris.net.ChuckNorrisService;
import com.pij.chucknorris.net.Record;
import com.pij.chucknorris.net.Response;

import butterknife.ButterKnife;
import butterknife.InjectView;
import icepick.Icepick;
import retrofit.RestAdapter;
import rx.Observable;
import rx.Subscription;
import rx.android.app.RxFragment;


/**
 * Displays a joke passed during instantiation.
 */
public class JokeFragment extends RxFragment {

    private static final String ARG_JOKEID = "jokeId";

    @InjectView(R.id.joke)
    TextView joke;
    private ChuckNorrisService service;
    private Subscription jokeSubscription;

    public JokeFragment() {
    }

    public static JokeFragment newInstance(int jokeId) {
        JokeFragment result = new JokeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_JOKEID, jokeId);
        result.setArguments(args);
        return result;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint("http://api.icndb.com")
                .build();

        service = restAdapter.create(ChuckNorrisService.class);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        service = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_joke, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        Icepick.restoreInstanceState(this, savedInstanceState);

        if (savedInstanceState == null) {
            if (hasJokeId()) {
                // start loading the joke
                Observable<Response<Record>> jokeRetriever = service.getJoke(getJokeId()).compose(Util.<Response<Record>>netScheduler());
                //noinspection Convert2MethodRef
                jokeSubscription = jokeRetriever.subscribe(
                        response -> setJokeText(response.getValue().getJoke()),
                        error -> signal(error));
            }
        }
    }

    private void signal(Throwable error) {
        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
        Log.d(getClass().getSimpleName(), "Could not access Chuck Norris database.", error);
    }

    private void setJokeText(String text) {
        joke.setText(text);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);

        unsubscribe(jokeSubscription);
        jokeSubscription = null;
    }

    private void unsubscribe(Subscription subscription) {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    private boolean hasJokeId() {
        return getArguments() != null && getArguments().getInt(ARG_JOKEID, -1) != -1;
    }

    private int getJokeId() {
        return getArguments().getInt(ARG_JOKEID);
    }

}
